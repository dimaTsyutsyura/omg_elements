using Controller.Game;
using Game.GameUtils;
using Model;
using UnityEngine;

public class EntryPoint : MonoBehaviour
{
    [SerializeField] public BoardGameObjectsPool _pool;
    [SerializeField] public GameProgressControllerBase _progressController;

    private IContentLoader _contentLoader;

    private void Awake()
    {
        //Check existing all reference objects.
        if (_pool == null || _progressController == null)
        {
            Debug.LogError("Check Inspector. You have to fill all fields in EntryPoint Component. For example IBoardGameObjectsPool or IGameProgressController");
        }
        else
        {
            //Try to create pool of gameObjects.
            _pool.InitializePool(OnPoolInitializeComplete);
        }
    }

    private void OnPoolInitializeComplete()
    {
        //Get levels configuration data from file or from server. 
        _contentLoader = new ContentLoaderFromLocalFile();
        _contentLoader.GetLevels(OnLoadLevelsCallback);
    }

    private void OnLoadLevelsCallback(ILevel[] obj)
    {
        //Check what we got with levels and go next step.
        if (obj != null && obj.Length > 0)
        {
            _progressController.SetGameParamsAndStart(_pool, obj); 
        }
        else
        {
            Debug.LogError("Something wrong with loading levels. Levels array is null or empty");
        }
    }
}