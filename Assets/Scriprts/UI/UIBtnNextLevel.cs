using GEvents;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Button))]
    public class UIBtnNextLevel:MonoBehaviour
    {
        private Button _btn;

        private void Start()
        {
            _btn = GetComponent<Button>();
            _btn.onClick.AddListener(OnBtnClick);
        }

        private void OnBtnClick()
        {
            GEvents.GEvents.Report(new AllHeaderGlobalEvents.GE_Game.NextLevel(), this);
        }
    }
}