namespace Scriprts.Singleton
{
    public abstract class Singleton<T> : SingletonMonoBehaviourBase
        where T : SingletonMonoBehaviourBase
     {
        private static object _lockObj = new object();
        private static bool _isInit;
        private bool _isDestroyed;

        private static bool _createdGameObj = true;

        private static T _instance = null;

        public static bool Exist
        {
            get {
                return _instance != null;
            }
        } 

        public static T Instance
        {
            get
            {
                if (_instance == null)
					return CheckInstance();
                return _instance;
            }
        }

        protected sealed override void Awake()
        {
            CheckInstance();
        }

        public override void Init()
        {
        }

       
        
        protected sealed override void OnDestroy()
        {
            _instance = null;
            Dispose();
            if (!_isDestroyed)
            {
                _isInit = false;
                _isDestroyed = true;
                if (_createdGameObj)
                {
                    Destroy(gameObject);
                }
                else
                {
                    Destroy(this);
                }
            }
        }

        public override void Dispose()
        {
            _instance = null;
            if (!_isDestroyed)
            {
                _isInit = false;
                _isDestroyed = true;
                if (_createdGameObj)
                {
                    Destroy(gameObject);
                }
                else if (UnityEngine.Application.isPlaying)
                {
                    Destroy(this);
                }
            }
        }

        private static T CheckInstance()
        {
            lock (_lockObj)
            {
                T[] list = FindObjectsOfType<T>();
                if (list != null && list.Length > 0)
                {
                    for (int i = 0; i < list.Length; i++)
                    {
                        if (_instance != null && _instance != list[i])
                            Destroy(list[i].gameObject);
                        if (_instance == null)
                        {
                            CreateSingleton(list[i]);
                        }
                    }
                }
                else
                    CreateSingleton(null);

				return _instance;
            }
        }

        private static void CreateSingleton(T instance)
        {
            lock (_lockObj)
            {
                if (instance != null)
                {
                    _instance = instance;
                    if (!_isInit)
                    {
                        _instance.Init();
                        _isInit = true;
                    }
                    return;
                }
            }
        }
    }
}