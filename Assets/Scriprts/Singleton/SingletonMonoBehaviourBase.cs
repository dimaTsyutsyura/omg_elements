using UnityEngine;

namespace Scriprts.Singleton
{
    public abstract class SingletonMonoBehaviourBase : MonoBehaviour
    {
        public abstract void Init();
        public abstract void Dispose();

        protected virtual void Awake()
        {
            
        }
        protected virtual void OnDestroy()
        {
            
        }
    }
}