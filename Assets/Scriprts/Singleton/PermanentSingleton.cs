using UnityEngine;

namespace Scriprts.Singleton
{
    public abstract class PermanentSingleton<T> : SingletonMonoBehaviourBase
        where T : SingletonMonoBehaviourBase
   {
        private static object _lockObj = new object();

        public static bool IsDestroed = false;
        private static bool _isInit;

        private static T _instance = null;
        public static bool Exist
        {
            get {
                return _instance != null;
            }
        }

        public static T Instance
        {
            get
            {
                if (_instance == null)
                    return CheckInstance();
                return _instance;
            }
        }
        
        public static T GetInstance()
        {
            return _instance;
        }

        protected sealed override void Awake()
        {
            CheckInstance();
        }

        public override void Init()
        {
            
        }

        public static bool IsInitied()
        {
            return (_instance != null);
        }

        public static void InstantiateSingleton()
        {
            if(Instance == null) {
                Debug.LogError("Error Instantiate "+typeof(T));
            }
        }

	    private static T CheckInstance()
        {
            lock (_lockObj)
            {
                T[] list = FindObjectsOfType<T>();
                if (list != null && list.Length > 0)
                {
                    for (int i = 0; i < list.Length; i++)
                    {
                        if(_instance != null && _instance != list[i])
                            DestroyRecursive(list[i].transform);
                        if (_instance == null)
                        {
                            CreateSingleton(list[i]);
                        }
                    }
                }
                else
                    return CreateSingleton(null);

                return _instance;
            }
        }

        private static T CreateSingleton(T instance)
        {
            lock (_lockObj)
            {
                if (instance != null)
                {
                    _instance = instance;
                    SetNotDestroyedObject(_instance.transform);
                    if (!_isInit)
                    {
                        _instance.Init();
                        _isInit = true;
                    }
                    return _instance;
                }

                if (!IsDestroed)
                {
                    GameObject obj = new GameObject(typeof (T).Name);
                    _instance = obj.AddComponent<T>();
                    SetNotDestroyedObject(obj.transform);
                    if (!_isInit)
                    {
                        _instance.Init();
                        _isInit = true;
                    }
                }
                return _instance;
            }
        }

        private static void SetNotDestroyedObject(Transform currenTransform)
        {
            if (!UnityEngine.Application.isPlaying) return;
            DontDestroyOnLoad(currenTransform.gameObject);
            if(currenTransform.parent != null)
                SetNotDestroyedObject(currenTransform.parent);
        }

        protected sealed override void OnDestroy()
        {
            if (!UnityEngine.Application.isPlaying) return;
            _instance = null;
            Dispose();
            _isInit = false;
            DestroyRecursive(transform);
            Destroy(gameObject);
            IsDestroed = true;
        }

        public override void Dispose()
        {
            if (!UnityEngine.Application.isPlaying) return;
            Destroy(gameObject);
        }

        private static void DestroyRecursive(Transform currenTransform)
        {
            if (!UnityEngine.Application.isPlaying) return;
            if (currenTransform.parent != null)
                DestroyRecursive(currenTransform.parent);

            Destroy(currenTransform.gameObject);
        }
    }
}