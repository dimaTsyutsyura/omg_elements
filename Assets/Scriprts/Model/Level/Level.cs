﻿namespace Model
{
    public class Level : ILevel
    {
        public int LeveId { get; }
        public IBoardElement[,] BoardElements { get; }

        public Level(int levelId, IBoardElement[,] boardElements)
        {
            this.LeveId = levelId;
            this.BoardElements = boardElements;
        }
    }
}
