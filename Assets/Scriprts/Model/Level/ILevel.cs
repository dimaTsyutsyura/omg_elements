namespace Model
{
    public interface ILevel
    {
        int LeveId { get; }
        IBoardElement[,] BoardElements { get; }
        
        //Boosters, specialParams, ... 
    }
}