﻿using Game.GameUtils;

namespace Model
{
    public interface IBoardElement
    {
        int X { get; set; }
        int Y { get; set; }
        GameConsts.BoardElementType Type { get; set; }
    }
}