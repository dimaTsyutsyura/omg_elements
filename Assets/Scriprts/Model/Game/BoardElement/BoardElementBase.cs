﻿using Game.GameUtils;
using UnityEngine;

namespace Model
{
    public class BoardElementBase : IBoardElement
    {
        public int X { get; set; }
        public int Y { get; set; }
        public GameConsts.BoardElementType Type { get; set; }

        public BoardElementBase(int x, int y, GameConsts.BoardElementType type)
        {
            this.X = x;
            this.Y = y;
            this.Type = type;
        }
    }
}
