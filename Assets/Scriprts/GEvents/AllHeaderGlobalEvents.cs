namespace GEvents
{
    public class AllHeaderGlobalEvents
    {
        public sealed class GE_Common
        {
            public sealed class LoadingHideComplete : GlobalEvent
            {
            }
            public sealed class ChangeShowLoading : GlobalEvent
            {
                public bool isShow = true;
            }
        }

        public sealed class GE_Game
        {
            public sealed class StartGame : GlobalEvent
            {
            }

            public sealed class NextLevel : GlobalEvent
            {
            }

            public sealed class EndGame : GlobalEvent
            {
                public bool isWin;
                public int level;
            }
        }
    }
}