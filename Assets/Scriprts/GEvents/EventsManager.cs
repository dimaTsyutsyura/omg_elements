using System;
using System.Collections.Generic;
using UnityEngine;

namespace GEvents
{
     public class EventBase
    {
        public object sender = null;
    }

    public class EventsManager<TEvents> where TEvents : EventBase
    {
        public delegate void EventDelegate<T>(T e) where T : TEvents;
        private delegate void EventDelegate(TEvents e);

        private Dictionary<System.Type , EventDelegate> _delegates = new Dictionary<System.Type , EventDelegate>();
        private Dictionary<System.Delegate , EventDelegate> _delegateLookup = new Dictionary<System.Delegate , EventDelegate>();
        private Dictionary<System.Type , List<System.Delegate>> _connectDelegate = new Dictionary<System.Type , List<System.Delegate>>();

        public void AddListener<T>(EventDelegate<T> del) where T : TEvents
        {
            if (_delegateLookup.ContainsKey(del))
                return;

            EventDelegate internalDelegate = (e) => {
                if(del != null && del.Target != null && !del.Target.Equals(null))
                {
                    del((T)e);
                }
            };

            _delegateLookup[del] = internalDelegate;

            EventDelegate tempDel;
            if (_delegates.TryGetValue(typeof(T) , out tempDel)) {
                _delegates[typeof(T)] = tempDel += internalDelegate;
            } else {
                _delegates[typeof(T)] = internalDelegate;
            }
            if (!_connectDelegate.ContainsKey(typeof(T))) {
                _connectDelegate[typeof(T)] = new List<System.Delegate>();
            }
            _connectDelegate[typeof(T)].Add(del);
        }

        public void RemoveListener<T>(EventDelegate<T> del) where T : TEvents
        {
            EventDelegate internalDelegate;
            if (_delegateLookup.TryGetValue(del , out internalDelegate)) {
                EventDelegate tempDel;
                if (_delegates.TryGetValue(typeof(T) , out tempDel)) {
                    tempDel -= internalDelegate;
                    if (tempDel == null) {
                        _delegates.Remove(typeof(T));
                        _connectDelegate.Remove(typeof(T));
                    } else {
                        _delegates[typeof(T)] = tempDel;
                        _connectDelegate[typeof(T)].Remove(del);
                    }
                }
                _delegateLookup.Remove(del);
            }
        }

        public void Report(object sender, TEvents e)
        {
            EventDelegate del;
            if (_delegates.TryGetValue(e.GetType() , out del)) {
                e.sender = sender;
                try
                {
                    del.Invoke(e);
                } catch(Exception err)
                {
                    Debug.LogException(err);
                }
            }
        }
    }
}