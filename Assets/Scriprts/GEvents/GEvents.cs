using Scriprts.Singleton;

namespace GEvents
{
    public class GlobalEvent: EventBase
    {
    }
    
    public sealed class GEvents: PermanentSingleton<GEvents>
    {
        private EventsManager<GlobalEvent> _eventContainer = new EventsManager<GlobalEvent>();

        public static void AddListener<T>(EventsManager<GlobalEvent>.EventDelegate<T> del) where T : GlobalEvent
        {
            if(Instance) {
                Instance._eventContainer.AddListener<T>(del);
            } else {
                UnityEngine.Debug.LogError("Do not instance GEvents");
            }
        }

        public static void RemoveListener<T>(EventsManager<GlobalEvent>.EventDelegate<T> del) where T : GlobalEvent
        {
            if (Exist) {
                Instance._eventContainer.RemoveListener<T>(del);
            } else {
            }
        }

        public static void Report(GlobalEvent e, object sender = null)
        {
            if(Instance) {
                Instance._eventContainer.Report(sender , e);
            }
        }
    }
}