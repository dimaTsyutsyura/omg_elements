﻿using Controller.Levels;
using Game;
using Game.GameUtils;
using GEvents;
using Model;
using UnityEngine;

namespace Controller.Game
{
    public class GameProgressControllerBase : MonoBehaviour
    {
        [SerializeField] private GameBoard _currentGameBoard;
        private IBoardGameObjectsPool _boardGameObjectsPool;
        private ILevelsController _levelsController;
        
        private int _currentLevel;
        private int _objectOnLevelCount;
        private bool _canLoadNewLevel;
        
        /// <summary>
        /// Initialize func. to same all dependencies and start first level.
        /// </summary>
        /// <param name="objectsPool">referance to pool controler</param>
        /// <param name="allLevel">all levels database</param>
        public void SetGameParamsAndStart(IBoardGameObjectsPool objectsPool, ILevel[] allLevel)
        {
            _canLoadNewLevel = false;
            _currentLevel = 1;
            _boardGameObjectsPool = objectsPool;
            _levelsController = new LevelsControllerBase(allLevel);

            StartLevel();
        }
     
        private void Start()
        {
            if (_currentGameBoard != null)
            {
                _currentGameBoard.OnGameBoardElementHide += OnGameBoardElementHideHandler;
                GEvents.GEvents.AddListener<AllHeaderGlobalEvents.GE_Game.NextLevel>(OnNextLevelHandler);
            }
            else
            {
                Debug.LogError("Check Inspector. You have to fill all fields in GameProgressControllerBase Component. For example GameBoard");
            }
        }

        private void StartLevel()
        {
            _boardGameObjectsPool.CreateBoardGameObjects(
                _levelsController.GetAllBoardElementsByLevel(_currentLevel), 
                OnGetBoardElementsCallback);
        }
        
        private void OnGetBoardElementsCallback(IBoardGameElement[,] boardGameElements)
        {
            int x = boardGameElements.GetLength(0);
            int y = boardGameElements.GetLength(1);
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (boardGameElements[i, j] != null)
                    {
                        _objectOnLevelCount++;
                    }
                }
            }
            _currentGameBoard.FillGameBoard(boardGameElements, _objectOnLevelCount, OnFillBoardCallback);
        }

        private void OnFillBoardCallback()
        {
            _canLoadNewLevel = true;
        }

        private void OnNextLevelHandler(AllHeaderGlobalEvents.GE_Game.NextLevel e)
        {
            if (_canLoadNewLevel)
            {
                _canLoadNewLevel = false;
                _currentGameBoard.ForceClearGameBoard();
            }
        }

        private void OnGameBoardElementHideHandler(IBoardGameElement obj)
        {
            _boardGameObjectsPool.PutInPool(obj);
            _objectOnLevelCount--;
            if (_objectOnLevelCount == 0)
            {
                _canLoadNewLevel = false;
                StartNextLevel();
            }
        }
        
        void StartNextLevel()
        {
            _currentLevel = (_currentLevel) % _levelsController.AllLevelsCount();
            _currentLevel++;
            StartLevel(); 
        }
    }
}