using Model;

namespace Controller.Levels
{
    public class LevelsControllerBase:ILevelsController
    {
        private ILevel[] _currentGameLevels;

        public IBoardElement[,] GetAllBoardElementsByLevel(int level)
        {
            if (_currentGameLevels != null && _currentGameLevels.Length > 0)
            {
                for (var i = 0; i < _currentGameLevels.Length; i++)
                {
                    if (_currentGameLevels[i].LeveId == level)
                    {
                        return _currentGameLevels[i].BoardElements;
                    }
                }
            }
            return null;
        }

        public int AllLevelsCount()
        {
            return _currentGameLevels.Length;
        }

        public LevelsControllerBase(ILevel[] currentGameLevels)
        {
            _currentGameLevels = currentGameLevels;
        }
    }
}