using Model;

namespace Controller.Levels
{
    public interface ILevelsController
    {
        IBoardElement[,] GetAllBoardElementsByLevel(int level);
        int AllLevelsCount();
    }
}