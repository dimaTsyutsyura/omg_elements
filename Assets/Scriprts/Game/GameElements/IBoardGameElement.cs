using System;
using Game.GameUtils;
using Model;
using UnityEngine;

namespace Game
{
    public interface IBoardGameElement
    {
        void Activate(Action callback, Action<IBoardGameElement, GameConsts.SwipeDirection> swipeEvent);
        void SetPosition(Vector3 position, Transform parent);
        void SetPosition(Vector3 position);
        void SetPosition(Vector3 position, bool isAnimated, Action callback);
        void SetPosition(Vector3 position, Transform parent, bool isAnimated, Action callback);
        void Hide(Action<IBoardGameElement> action);
        void UpdateCoordinates(Vector2 coordinate);
        Vector2 GetCoordinate();
        void SetParams(IBoardElement element);
        GameConsts.BoardElementType GetElementType();
        GameObject GetGameObject();
        void Deactivate(Transform parent);
    }
}