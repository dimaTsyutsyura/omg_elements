using System;
using System.Collections;
using Game.GameUtils;
using Model;
using UnityEngine;

namespace Game
{
    public class BoardGameElementBase : MonoBehaviour, IBoardGameElement
    {
         [SerializeField] protected Animator _animator;
         [SerializeField] public GameConsts.BoardElementType PrefabType;
         [SerializeField] private SwipeDetecter swipeDetecter;

         [SerializeField] private float _animationSpeed = 1f;
         private IBoardElement _element { get; set;}
         
         private event Action<IBoardGameElement, GameConsts.SwipeDirection> OnBoardElementSwipedEvent;
         private event Action<IBoardGameElement> OnDestroyEndAnimation;
         
         public void Start()
         {
             if (swipeDetecter != null)
             {
                 swipeDetecter.OnSwipe += OnSwipe;
             }
         }

         public void Activate(Action callback, Action<IBoardGameElement, GameConsts.SwipeDirection> swipeEvent)
         {
             OnBoardElementSwipedEvent = swipeEvent;
             gameObject.SetActive(true);
             string triger = GameConsts.BoardElementAnimationTriggers.Idle.ToString();
             _animator.SetTrigger(triger);
             callback?.Invoke();
         }

         public void SetPosition(Vector3 position, Transform parent)
         {
             transform.SetParent(parent);
             transform.position = position;
         }

         public void SetPosition(Vector3 position)
         {
             transform.position = position;
         }

         public void SetPosition(Vector3 position, bool isAnimated, Action callback)
         {
             StartCoroutine(SetAnimatedPosition(position, callback));
         }

         public void SetPosition(Vector3 position, Transform parent, bool isAnimated, Action callback)
         {
             transform.SetParent(parent);
             StartCoroutine(SetAnimatedPosition(position, callback));
         }

         public void Hide(Action<IBoardGameElement> callback)
         {
             StopAllCoroutines();
             OnDestroyEndAnimation = callback;
             string triger = GameConsts.BoardElementAnimationTriggers.Destroy.ToString();
             _animator.SetTrigger(triger);
         }

         public void UpdateCoordinates(Vector2 coordinate)
         {
             _element.X = (int) coordinate.x;
             _element.Y = (int) coordinate.y;
         }

         public void SetParams (IBoardElement element)
         {
             _element = element;
         }

         public GameConsts.BoardElementType GetElementType()
         {
             if (_element != null)
             {
                 return _element.Type;
             }
             return GameConsts.BoardElementType.Empty;
         }

         public GameObject GetGameObject()
         {
             return gameObject;
         }

         public void Deactivate(Transform parent)
         {
             OnBoardElementSwipedEvent = null;
             OnDestroyEndAnimation = null;
             StopAllCoroutines();
             gameObject.SetActive(false);
             transform.SetParent(parent);
         }

         public Vector2 GetCoordinate()
         {
             return new Vector2(_element.X, _element.Y);
         }
            
         public void OnEndDestroyAnimCallback()
         {
             OnDestroyEndAnimation?.Invoke(this);
         }

         private void OnSwipe(GameConsts.SwipeDirection obj)
         {
             OnBoardElementSwipedEvent?.Invoke(this, obj);
         }

         private IEnumerator SetAnimatedPosition(Vector3 targetPosition, Action action)
         {
             while (Vector3.Distance(transform.position ,targetPosition) > 0.1f)
             {
                 yield return new WaitForFixedUpdate();   
                 transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * _animationSpeed);
             }
             yield return new WaitForFixedUpdate();  
             action?.Invoke();
         }
    }
 }