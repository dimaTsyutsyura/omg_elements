using System;
using System.Collections.Generic;
using Game.GameUtils;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// reference - https://habr.com/ru/post/329016/
    /// </summary>
    public class BoardAnalyzer
    {
        private int _curerentRows;
        private int _currentColums;

        private IBoardGameElement _currentTile;
        private List<IBoardGameElement> _collector = new List<IBoardGameElement>();
        private IBoardGameElement[,] _tmpArray;

        public void Match(IBoardGameElement[,] board, Action<List<IBoardGameElement>> callback)
        {
            List<IBoardGameElement> matchedElements = new List<IBoardGameElement>();
            _currentTile = null;
            _collector.Clear();
            
            _curerentRows = board.GetLength(0);
            _currentColums = board.GetLength(1);
            
            CopyBoards(board);
            //GetAllHorizontalMatches
            for (var y = 0; y < _currentColums; y++)
            {
                for (var x = 0; x < _curerentRows; x++)
                {
                    TestTile(x, y, true);
                    if (_collector.Count >= 3)
                    {
                        foreach (var boardGameElement in _collector)
                        {
                            if (!matchedElements.Contains(boardGameElement))
                            {
                                matchedElements.Add(boardGameElement);
                            }
                        }
                    }
                    _currentTile = null;
                    _collector.Clear ();
                }
            }
            
            CopyBoards(board);
            //GetAllVerticalsMatches
            for (var x = 0; x < _curerentRows; x++)
            {
                for (var y = 0; y < _currentColums; y++)
                {
                    TestTile(x, y, false);
                    if (_collector.Count >= 3)
                    {
                        foreach (var boardGameElement in _collector)
                        {
                            if (!matchedElements.Contains(boardGameElement))
                            {
                                matchedElements.Add(boardGameElement);
                            }
                        }
                    }
                    _currentTile = null;
                    _collector.Clear ();
                }
            }
            callback?.Invoke(matchedElements);
        }

        public void SettleBlocks(IBoardGameElement[,] board, Action<Dictionary<IBoardGameElement, Vector2>> callback)
        {
            int? firstEmpty;
            Dictionary<IBoardGameElement, Vector2> fallingBlocks = new Dictionary<IBoardGameElement, Vector2>();
            _curerentRows = board.GetLength(0);
            _currentColums = board.GetLength(1);
            
            for (var x = 0; x < _curerentRows; x++)
            {
                firstEmpty = null;
                for (var y = 0; y < _currentColums; y++)
                {
                    if (board[x, y] == null && !firstEmpty.HasValue || 
                        board[x, y] != null && board[x, y].GetElementType() == GameConsts.BoardElementType.Empty && !firstEmpty.HasValue)
                    {
                        firstEmpty = y;
                    }
                    else if (firstEmpty.HasValue && board[x, y] != null && board[x,y].GetElementType() != GameConsts.BoardElementType.Empty)
                    {
                        Vector2 newPos = new Vector2(x, firstEmpty.Value);
                        fallingBlocks.Add(board[x, y], newPos);
                        board[x, firstEmpty.Value] = board[x, y];
                        board[x, y] = null;
                        firstEmpty++;
                    }
                }
            }
            
            callback?.Invoke(fallingBlocks);
        }

        void CopyBoards(IBoardGameElement[,] source)
        {
            _tmpArray = new IBoardGameElement[_curerentRows, _currentColums];
            for (int y = 0; y < _currentColums; y++)
            {
                for (int x = 0; x < _curerentRows; x++)
                {
                    _tmpArray[x, y] = source[x, y];
                }
            }
        }
        
        void TestTile(int x, int y, bool isHorizontal)
        {
            // Start testing a block
            if (_tmpArray[x,y] == null ||  _tmpArray[x,y].GetElementType() == GameConsts.BoardElementType.Empty)
            {
                return;
            }
            // Start testing a block
            if (_currentTile == null)
            {
                _currentTile = _tmpArray[x, y];
                _tmpArray[x, y] = null;
                _collector.Add(_currentTile);
            }
            // Tile doesn't match; skip
            else if (_currentTile.GetElementType() != _tmpArray[x, y].GetElementType())
            {
                return;
            }
            // Tile matches
            else
            {
                _collector.Add(_tmpArray[x, y]);
                _tmpArray[x, y] = null;
            }
            
            // If we're processing this tile, test all tiles around it

            if (isHorizontal)
            {
                if (x > 0)
                    TestTile(x - 1, y, isHorizontal);
                if (x < _curerentRows - 1)
                    TestTile(x + 1, y, isHorizontal);
            }
            else
            {
                if (y > 0)
                    TestTile(x, y - 1,isHorizontal);
    
                if (y < _currentColums - 1)
                    TestTile(x, y + 1, isHorizontal);
            }
        }
    }
}