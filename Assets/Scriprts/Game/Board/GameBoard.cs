﻿using System;
using System.Collections.Generic;
using Game.GameUtils;
using UnityEngine;

namespace Game
{
    public class GameBoard : MonoBehaviour
    {
        public Action<IBoardGameElement> OnGameBoardElementHide;

        [SerializeField] private CanvasBoardPositionConverter _boardPositionConverter;

        private BoardAnalyzer _boardAnalyzer;
        private IBoardGameElement[,] _currentElements;
        private int _currentRows;
        private int _currentColumns;
        
        private Vector2 _swipeFromCoordinates;
        private Vector2 _swipeToCoordinates;
        private bool _canSwipe = true;
        
        private void Start()
        {
            _boardAnalyzer = new BoardAnalyzer();
            if (_boardPositionConverter == null)
            {
                Debug.LogError("Check Inspector. You have to fill all fields in GameBoard Component. For example CanvasBoardPositionConverter");
            }
        }

        public void ForceClearGameBoard()
        {
            _canSwipe = false;
            for (int x = 0; x < _currentRows; x++)
            {
                for (int y = 0; y < _currentColumns; y++)
                {
                    if (_currentElements[x, y] != null)
                    {
                        _currentElements[x, y].Hide(element => { OnGameBoardElementHide?.Invoke(element); });
                    }
                }
            }
        }

        public void FillGameBoard(IBoardGameElement[,] elements, int objectsOnLevel ,Action callback)
        {
            int activateObjectCount = 0;
            if (elements != null && elements.Length > 0)
            {
                _currentElements = elements;
                _currentRows = elements.GetLength(0);
                _currentColumns = elements.GetLength(1);
                
                _boardPositionConverter.UpdateLayoutGrid(_currentRows,  _currentColumns, () =>
                {
                    for (int y = 0; y < _currentColumns; y++)
                    {
                        for (int x = 0; x < _currentRows; x++)
                        {
                            if (elements[x, y] != null &&
                                elements[x, y].GetElementType() != GameConsts.BoardElementType.Empty)
                            {
                                elements[x,y].SetPosition(_boardPositionConverter.GetPositionByCoordinate(x,y),_boardPositionConverter.GetParentByCoordinate(x,y) );
                                elements[x, y].Activate(() =>
                                {
                                    activateObjectCount++;     
                                    if (objectsOnLevel == activateObjectCount)
                                    {
                                        callback?.Invoke();
                                        _canSwipe = true;
                                    }
                                }, OnSwipeElementEvent);
                            }
                        }                
                    }
                });
            }
            else
            {
                Debug.LogWarning("Fill GameBoard ERROR. null or empty element array");
                callback?.Invoke();
            }
        }


        #region Main Game Loop
        private void OnSwipeElementEvent(IBoardGameElement gameElement, GameConsts.SwipeDirection direction)
        {
            if (_canSwipe)
            {
                _canSwipe = false;
                _swipeFromCoordinates = gameElement.GetCoordinate();
                _swipeToCoordinates = TryToGetNewCoordinates(gameElement.GetCoordinate(), direction);
                if (_swipeToCoordinates.x >= 0 && _swipeToCoordinates.y >= 0)
                {
                    Replace(_swipeFromCoordinates, _swipeToCoordinates, TryToSettle);
                }
                else
                {
                    _canSwipe = true;
                }
            }
        }
        
        private void TryToSettle()
        {
            _boardAnalyzer.SettleBlocks(_currentElements, OnSettleBlocksCallback);
        }
        
        private void OnSettleBlocksCallback(Dictionary<IBoardGameElement, Vector2> fallingBlocks)
        {
            if (fallingBlocks.Count > 0)
            {
                MoveObjects(fallingBlocks, TryToMatch);
            }
            else
            {
                TryToMatch();
            }
        }

        private void TryToMatch()
        {
            _boardAnalyzer.Match(_currentElements, OnMathCallback);
        }
        
        private void OnMathCallback(List<IBoardGameElement> matchedElements)
        {
            if (matchedElements.Count > 0)
            {
                RemoveMatchedElements(matchedElements, TryToSettle);
            }
            else
            {
                _canSwipe = true;
            }
        }
        #endregion
       
        private void Replace(Vector2 fromPos, Vector2 toPos, Action callback)
        {
            IBoardGameElement fromObject = _currentElements[(int) fromPos.x, (int) fromPos.y];
            IBoardGameElement toObject = _currentElements[(int) toPos.x, (int) toPos.y];
            int objectsCount = 2;
            Action setPositionCallback = delegate
            {
                objectsCount--;
                if (objectsCount == 0)
                {
                    _currentElements[(int) fromPos.x, (int) fromPos.y] = toObject;
                    _currentElements[(int) toPos.x, (int) toPos.y] = fromObject;
                    callback?.Invoke();
                }
            };
            MoveObject(fromObject, toPos, setPositionCallback);
            MoveObject(toObject, fromPos, setPositionCallback);
        }

        private void MoveObject(IBoardGameElement gameElement, Vector2 toPos, Action callback)
        {
            if (gameElement == null)
            {
                callback?.Invoke();
            }
            else
            {
                gameElement.UpdateCoordinates(toPos);
                gameElement.SetPosition(_boardPositionConverter.GetPositionByCoordinate(toPos), _boardPositionConverter.GetParentByCoordinate(toPos), true, () =>
                {
                    callback?.Invoke();
                });
            }
        }

        private void MoveObjects(Dictionary<IBoardGameElement, Vector2> movedObjects, Action callback)
        {
            var movedCount = movedObjects.Count;
            Action moveObjectCallback = delegate
            {
                movedCount--;
                if (movedCount == 0)
                {
                    callback?.Invoke();
                }
            };
            foreach (KeyValuePair<IBoardGameElement,Vector2> movedObject in movedObjects)
            {
                MoveObject(movedObject.Key, movedObject.Value, moveObjectCallback);
            }
        }

        private void RemoveMatchedElements(List<IBoardGameElement> matchedElements, Action callback)
        {
            int matchCount = matchedElements.Count;
            for (var i = 0; i < matchedElements.Count; i++)
            {
                Vector2 pos = matchedElements[i].GetCoordinate();
                matchedElements[i].Hide(element => 
                {
                    _currentElements[(int) pos.x, (int) pos.y] = null;
                    matchCount--;
                    if (matchCount == 0)
                    {
                        callback?.Invoke();
                    }
                    OnGameBoardElementHide?.Invoke(element);
                });
            }
        }

        private Vector2 TryToGetNewCoordinates(Vector2 coordinates, GameConsts.SwipeDirection direction)
        {
            switch (direction)
            {
                case GameConsts.SwipeDirection.Up:
                    var newY = (coordinates.y < _currentColumns-1) ? coordinates.y+1 : coordinates.y;
                    if (_currentElements[(int)coordinates.x, (int)newY] != null &&
                        _currentElements[(int)coordinates.x, (int)newY].GetElementType() != GameConsts.BoardElementType.Empty)
                    {
                        coordinates.y = newY;
                    }
                    break;
                case GameConsts.SwipeDirection.Down:
                    coordinates.y = (coordinates.y > 0) ? coordinates.y-1 : coordinates.y;
                    break;
                case GameConsts.SwipeDirection.Left:
                    coordinates.x = (coordinates.x > 0) ? coordinates.x-1 : coordinates.x;
                    break;
                case GameConsts.SwipeDirection.Right:
                    coordinates.x = (coordinates.x < _currentRows-1) ? coordinates.x+1 : coordinates.x;
                    break;
            }
            return coordinates;
        }
    }
}