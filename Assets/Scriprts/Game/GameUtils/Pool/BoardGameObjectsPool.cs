﻿using System;
using System.Collections.Generic;
using Model;
using UnityEngine;

namespace Game.GameUtils
{
    public class BoardGameObjectsPool : MonoBehaviour, IBoardGameObjectsPool
    {
        private const int DEFAULT_POOL_COUNT = 10; //initialize pool length
        private const int DEFAULT_POOL_OFFSET = 5; //increase pool count
        
        [SerializeField] private BoardGameElementBase[] _prefabs; 
        
        private Dictionary<GameConsts.BoardElementType, GameObject[]> _pool { get; set; }
        
        public void InitializePool(Action callback)
        {
            _pool = new Dictionary<GameConsts.BoardElementType, GameObject[]>();
            if (_prefabs.Length > 0)
            {
                for (var i = 0; i < _prefabs.Length; i++)
                {
                    if (_prefabs[i] != null && _prefabs[i].PrefabType != GameConsts.BoardElementType.Empty)
                    {
                        _pool.Add(_prefabs[i].PrefabType, new GameObject[DEFAULT_POOL_COUNT]);
                        CreatePoolObjects(_prefabs[i].PrefabType, _prefabs[i].gameObject, DEFAULT_POOL_COUNT);
                    }
                    else
                    {
                        Debug.LogWarning("BoardGameObjectsPool one of prefabs is empty or type is wrong");
                    }
                }
                callback?.Invoke();
            }
            else
            {
                Debug.LogError("BoardGameObjectsPool couldn't be initialized. Please set prefabs array");
            }
        }

        //
        // Attention - this func is recursive.
        //
        public GameObject TakeFromPool(GameConsts.BoardElementType type)
        {
            if (_pool.ContainsKey(type))
            {
                //Try to get from pool;
                for (int i = 0; i < _pool[type].Length; i++)
                {
                    if (_pool[type][i] != null)
                    {
                        GameObject result = _pool[type][i];
                        _pool[type][i] = null;
                        return result;
                    }
                }

                //If pool dont have available gameObjects of this type - Make array bigger and create new GameObjects
                _pool[type] = new GameObject[_pool[type].Length + DEFAULT_POOL_OFFSET];
                if(CreatePoolObjects(type, DEFAULT_POOL_OFFSET))
                {
                    return TakeFromPool(type);
                }
            }
            else if(type != GameConsts.BoardElementType.Empty)
            {
                Debug.LogError($"TakeFromPool() ERROR. POOL DOES NOT CONTAINS OBJECTS OF TYPE - {type.ToString()} and couldn't create new");
            }
            return null;
        }

        public bool PutInPool(IBoardGameElement boardGameElement)
        {
            GameConsts.BoardElementType type = boardGameElement.GetElementType();
            if (_pool.ContainsKey(type))
            {
                for (int i = 0; i < _pool[type].Length; i++)
                {
                    if (_pool[type][i] == null)
                    {
                        boardGameElement.Deactivate(transform);
                        _pool[type][i] = boardGameElement.GetGameObject();
                        return true;
                    }
                }
                Debug.LogWarning($"Something going wrong with putting to pool gameObject of type - {type.ToString()}");
            }
            else
            {
                Debug.LogError($"PutInPool() ERROR. POOL DOES NOT CONTAINS ARRAY OF TYPE - {type.ToString()}");
            }
            return false;
        }

        public void CreateBoardGameObjects(IBoardElement[,] boardElements, Action<IBoardGameElement[,]> callback)
        {
            if (boardElements.Length > 0)
            {
                int rows = boardElements.GetUpperBound(0) + 1;
                int columns = boardElements.Length / rows;
            
                IBoardGameElement[,] result = new IBoardGameElement[rows,columns];
                for (int x = 0; x < rows; x++)
                {
                    for (int y = 0; y <columns; y++)
                    {
                        if (boardElements[x, y] != null)
                        {
                            GameObject go = TakeFromPool(boardElements[x, y].Type);
                            if (go != null)
                            {
                                BoardGameElementBase boardGameElement = go.GetComponent<BoardGameElementBase>();
                                if (boardGameElement != null)
                                {
                                    boardGameElement.SetParams(boardElements[x,y]);
                                    boardGameElement.UpdateCoordinates(new Vector2(x,y));
                                    result[x, y] = boardGameElement;
                                }
                                else
                                {
                                    Debug.LogWarning("GameObject in pool dont have required Component - BoardGameElementBase. Please check your prefabs");
                                }
                            }
                        }
                    }
                }

                callback?.Invoke(result);
            }
            else
            {
                Debug.LogWarning("Something wrong! It looks like level dont have eny valid elements");
                callback?.Invoke(null);
            }
        }

        private void CreatePoolObjects(GameConsts.BoardElementType type, GameObject go, int count)
        {
            for (var j = 0; j < count; j++)
            {
                _pool[type][j] = Instantiate(go, transform);  
            }
        }
        
        private bool CreatePoolObjects(GameConsts.BoardElementType type, int count)
        {
            for (var i = 0; i < _prefabs.Length; i++)
            {
                if (_prefabs[i].PrefabType == type)
                {
                    for (var j = 0; j < count; j++)
                    {
                        _pool[type][j] = Instantiate(_prefabs[i].gameObject, transform);  
                    }
                    return true;
                }
            }
            return false;
        }
    }
}
