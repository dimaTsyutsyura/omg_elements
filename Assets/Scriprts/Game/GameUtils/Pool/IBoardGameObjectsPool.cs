using System;
using Model;
using UnityEngine;

namespace Game.GameUtils
{
    public interface IBoardGameObjectsPool
    {
        void InitializePool(Action callback);
        GameObject TakeFromPool(GameConsts.BoardElementType type);
        bool PutInPool(IBoardGameElement go);
        void CreateBoardGameObjects(IBoardElement[,] boardElements, Action<IBoardGameElement[,]> callback);
    }
}