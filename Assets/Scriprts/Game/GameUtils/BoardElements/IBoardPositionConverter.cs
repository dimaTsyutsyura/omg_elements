using System;
using UnityEngine;

namespace Game.GameUtils
{
    /// <summary>
    /// In our case we use quick decision - Canvas
    /// but in production we often use object in world camera, or 3d object
    /// in this case we have to implement other logic but with same interface
    /// </summary>
    public interface IBoardPositionConverter
    {
        void UpdateLayoutGrid(int x, int y, Action callback = null);
        Vector3 GetPositionByCoordinate(int x, int y);
        Vector3 GetPositionByCoordinate(Vector2 vector);
        
    }
}