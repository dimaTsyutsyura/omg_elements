using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Game.GameUtils
{
    public class CanvasBoardPositionConverter :MonoBehaviour, IBoardPositionConverter
    {
        [SerializeField] private GridLayoutGroup _gridLayoutGroup;
        
        private GameObject[,] _currentGripPool;
        private Action _updateLayoutCallback;
            
        public void Start()
        {
            if (_gridLayoutGroup == null)
            {
                Debug.LogError("Check Inspector. You have to fill all fields in CanvasBoardPositionConverter Component. For example GridLayoutGroup");
            }
        }

        public void UpdateLayoutGrid(int x, int y, Action callback = null)
        {
            _gridLayoutGroup.enabled = true;
            _updateLayoutCallback = callback;
            if (_currentGripPool != null)
            {
                foreach (GameObject go in _currentGripPool)
                {
                    if (go != null)
                    {
                        Destroy(go);
                    }
                }
            }
            _currentGripPool = new GameObject[x,y];
            _gridLayoutGroup.constraintCount = x;
            for (var i = 0; i < x; i++)
            {
                for (var j = 0; j < y; j++)
                {
                    _currentGripPool[i,j] = new GameObject($"X:{i}, Y:{j}");
                    _currentGripPool[i, j].AddComponent(typeof(RectTransform));
                    _currentGripPool[i,j].transform.SetParent(transform);
                }
            }
            StartCoroutine(CreateObjectAndSendCallback());
        }

        public Vector3 GetPositionByCoordinate(int x, int y)
        {
            if (_currentGripPool != null && _currentGripPool[x,y] !=null)
            {
                return _currentGripPool[x, y].transform.position;
            }
            else
            {
                Debug.LogErrorFormat("Try to get position x:{0} y:{1}. But CanvasBoardPositionConverter gridPool is null or empty", x, y);
                return Vector3.zero;
            }
        }

        public Transform GetParentByCoordinate(int x, int y)
        {
            if (_currentGripPool != null && _currentGripPool[x,y] !=null)
            {
                return _currentGripPool[x, y].transform;
            }
            else
            {
                Debug.LogErrorFormat("Try to get position x:{0} y:{1}. But CanvasBoardPositionConverter gridPool is null or empty", x, y);
                return this.transform;
            }
        }

        public Vector3 GetPositionByCoordinate(Vector2 vector)
        {
            return GetPositionByCoordinate((int) vector.x, (int) vector.y);
        }
        public Transform GetParentByCoordinate(Vector2 vector)
        {
            return GetParentByCoordinate((int) vector.x, (int) vector.y);
        }
        
        private IEnumerator CreateObjectAndSendCallback()
        {
            yield return  new WaitForEndOfFrame();
            _gridLayoutGroup.enabled = false;
            if (_updateLayoutCallback != null)
            {
                _updateLayoutCallback();
            }
        }
    }
}