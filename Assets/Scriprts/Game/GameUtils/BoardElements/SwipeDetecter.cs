using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.GameUtils
{
    public class SwipeDetecter : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private float _minDistanceForSwipe = 20f;
        
        private Vector2 _fingerDownPosition;
        private Vector2 _fingerUpPosition;
        
        public event Action<GameConsts.SwipeDirection> OnSwipe;

        public void OnPointerDown(PointerEventData eventData)
        {
            _fingerDownPosition = eventData.position;
        }
        public void OnPointerUp(PointerEventData eventData)
        {
            _fingerUpPosition = eventData.position;
            DetectSwipe();
        }
        
        private void DetectSwipe()
        {
            if (SwipeDistanceCheckMet())
            {
                if (IsVerticalSwipe())
                {
                    var direction = _fingerDownPosition.y - _fingerUpPosition.y > 0 ? GameConsts.SwipeDirection.Down : GameConsts.SwipeDirection.Up;
                    if (OnSwipe != null)
                    {
                        OnSwipe(direction);
                    }
                }
                else
                {
                    var direction = _fingerDownPosition.x - _fingerUpPosition.x > 0 ? GameConsts.SwipeDirection.Left : GameConsts.SwipeDirection.Right;
                    if (OnSwipe != null)
                    {
                        OnSwipe(direction);
                    }
                }
                _fingerUpPosition = _fingerDownPosition;
            }
        }
        private bool IsVerticalSwipe()
        {
            return VerticalMovementDistance() > HorizontalMovementDistance();
        }
        private bool SwipeDistanceCheckMet()
        {
            return VerticalMovementDistance() > _minDistanceForSwipe || HorizontalMovementDistance() > _minDistanceForSwipe;
        }
        private float VerticalMovementDistance()
        {
            return Mathf.Abs(_fingerDownPosition.y - _fingerUpPosition.y);
        }
        private float HorizontalMovementDistance()
        {
            return Mathf.Abs(_fingerDownPosition.x - _fingerUpPosition.x);
        }
    }
}