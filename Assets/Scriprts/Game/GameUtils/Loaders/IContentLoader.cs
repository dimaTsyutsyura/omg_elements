using System;
using Model;

namespace Game.GameUtils
{
    public interface IContentLoader
    {
        void GetLevels(Action<ILevel[]> callback);
    }
}