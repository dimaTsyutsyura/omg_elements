/*
Json file will be in format - 

[{"id":1,"boardElements":[[{"x":0,"y":0,"type":"Water"},{"x":0,"y":1,"type":"Water"}],[{"x":1,"y":0,"type":"Empty"},{"x":1,"y":1,"type":"Empty"}],[{"x":2,"y":0,"type":"Water"},{"x":2,"y":1,"type":"Fire"}],[{"x":3,"y":0,"type":"Fire"}],[{"x":4,"y":0,"type":"Fire"}]]},{"id":2,"boardElements":[[{"x":0,"y":0,"type":"Water"},{"x":0,"y":1,"type":"Water"},{"x":0,"y":2,"type":"Fire"},{"x":0,"y":3,"type":"Water"},{"x":0,"y":4,"type":"Water"}],[{"x":1,"y":0,"type":"Fire"},{"x":1,"y":1,"type":"Fire"},{"x":1,"y":2,"type":"Water"},{"x":1,"y":3,"type":"Fire"},{"x":1,"y":4,"type":"Water"}],[{"x":2,"y":0,"type":"Water"},{"x":2,"y":1,"type":"Water"},{"x":2,"y":2,"type":"Fire"},{"x":2,"y":3,"type":"Water"},{"x":2,"y":4,"type":"Empty"}],[{"x":3,"y":0,"type":"Water"},{"x":3,"y":1,"type":"Water"},{"x":3,"y":2,"type":"Fire"},{"x":3,"y":3,"type":"Water"},{"x":3,"y":4,"type":"Empty"}]]},{"id":3,"boardElements":[[{"x":0,"y":0,"type":"Water"},{"x":0,"y":1,"type":"Water"},{"x":0,"y":2,"type":"Fire"},{"x":0,"y":3,"type":"Water"},{"x":0,"y":4,"type":"Water"}],[{"x":1,"y":0,"type":"Fire"},{"x":1,"y":1,"type":"Fire"},{"x":1,"y":2,"type":"Water"},{"x":1,"y":3,"type":"Water"},{"x":1,"y":4,"type":"Fire"},{"x":1,"y":5,"type":"Water"}],[{"x":2,"y":0,"type":"Water"},{"x":2,"y":1,"type":"Water"},{"x":2,"y":2,"type":"Fire"}],[{"x":3,"y":0,"type":"Water"},{"x":3,"y":1,"type":"Water"},{"x":3,"y":2,"type":"Fire"},{"x":3,"y":3,"type":"Water"}]]}]
*/


using System;
using Model;

namespace Game.GameUtils
{
    public class ContentLoaderFromLocalFile:IContentLoader
    {
        /// <summary>
        ///  Load all levels from JSON and send callback
        /// </summary>
        /// <param name="callback"></param>
        public void GetLevels(Action<ILevel[]> callback)
        {
            //
            //Temporary solution with hard set level;
            //
            
            ILevel[] levels = new ILevel[3];
            IBoardElement[,] boardElements = new IBoardElement[5,2];
                boardElements[0,0] = new BoardElementBase(0,0, GameConsts.BoardElementType.Water);
                boardElements[0,1] = new BoardElementBase(0,1, GameConsts.BoardElementType.Water);
                
                boardElements[1,0] = new BoardElementBase(1,0, GameConsts.BoardElementType.Empty);
                boardElements[1,1] = new BoardElementBase(1,1, GameConsts.BoardElementType.Empty);
                
                boardElements[2,0] = new BoardElementBase(2,0, GameConsts.BoardElementType.Water);
                boardElements[2,1] = new BoardElementBase(2,1, GameConsts.BoardElementType.Fire);
                
                boardElements[3,0] = new BoardElementBase(3,0, GameConsts.BoardElementType.Fire);
                
                boardElements[4,0] = new BoardElementBase(4,0, GameConsts.BoardElementType.Fire);
            levels[0] = new Level(1, boardElements);
                boardElements = new IBoardElement[4,5];
                boardElements[0,0] = new BoardElementBase(0,0, GameConsts.BoardElementType.Water);
                boardElements[0,1] = new BoardElementBase(0,1, GameConsts.BoardElementType.Water);
                boardElements[0,2] = new BoardElementBase(0,2, GameConsts.BoardElementType.Fire);
                boardElements[0,3] = new BoardElementBase(0,3, GameConsts.BoardElementType.Water);
                boardElements[0,4] = new BoardElementBase(0,4, GameConsts.BoardElementType.Water);

                boardElements[1,0] = new BoardElementBase(1,0, GameConsts.BoardElementType.Fire);
                boardElements[1,1] = new BoardElementBase(1,1, GameConsts.BoardElementType.Fire);
                boardElements[1,2] = new BoardElementBase(1,2, GameConsts.BoardElementType.Water);
                boardElements[1,3] = new BoardElementBase(1,3, GameConsts.BoardElementType.Fire);
                boardElements[1,4] = new BoardElementBase(1,4, GameConsts.BoardElementType.Water);

                boardElements[2,0] = new BoardElementBase(2,0, GameConsts.BoardElementType.Water);
                boardElements[2,1] = new BoardElementBase(2,1, GameConsts.BoardElementType.Water);
                boardElements[2,2] = new BoardElementBase(2,2, GameConsts.BoardElementType.Fire);
                boardElements[2,3] = new BoardElementBase(2,3, GameConsts.BoardElementType.Water);
                boardElements[2,4] = new BoardElementBase(2,4, GameConsts.BoardElementType.Empty);
                
                boardElements[3,0] = new BoardElementBase(3,0, GameConsts.BoardElementType.Water);
                boardElements[3,1] = new BoardElementBase(3,1, GameConsts.BoardElementType.Water);
                boardElements[3,2] = new BoardElementBase(3,2, GameConsts.BoardElementType.Fire);
                boardElements[3,3] = new BoardElementBase(3,3, GameConsts.BoardElementType.Water);
                boardElements[3,4] = new BoardElementBase(3,4, GameConsts.BoardElementType.Empty);
            levels[1] = new Level(2, boardElements);
                boardElements = new IBoardElement[4,6];
                boardElements[0,0] = new BoardElementBase(0,0, GameConsts.BoardElementType.Water);
                boardElements[0,1] = new BoardElementBase(0,1, GameConsts.BoardElementType.Water);
                boardElements[0,2] = new BoardElementBase(0,2, GameConsts.BoardElementType.Fire);
                boardElements[0,3] = new BoardElementBase(0,3, GameConsts.BoardElementType.Water);
                boardElements[0,4] = new BoardElementBase(0,4, GameConsts.BoardElementType.Water);

                boardElements[1,0] = new BoardElementBase(1,0, GameConsts.BoardElementType.Fire);
                boardElements[1,1] = new BoardElementBase(1,1, GameConsts.BoardElementType.Fire);
                boardElements[1,2] = new BoardElementBase(1,2, GameConsts.BoardElementType.Water);
                boardElements[1,3] = new BoardElementBase(1,3, GameConsts.BoardElementType.Water);
                boardElements[1,4] = new BoardElementBase(1,4, GameConsts.BoardElementType.Fire);
                boardElements[1,5] = new BoardElementBase(1,5, GameConsts.BoardElementType.Water);

                boardElements[2,0] = new BoardElementBase(2,0, GameConsts.BoardElementType.Water);
                boardElements[2,1] = new BoardElementBase(2,1, GameConsts.BoardElementType.Water);
                boardElements[2,2] = new BoardElementBase(2,2, GameConsts.BoardElementType.Fire);
                
                boardElements[3,0] = new BoardElementBase(3,0, GameConsts.BoardElementType.Water);
                boardElements[3,1] = new BoardElementBase(3,1, GameConsts.BoardElementType.Water);
                boardElements[3,2] = new BoardElementBase(3,2, GameConsts.BoardElementType.Fire);
                boardElements[3,3] = new BoardElementBase(3,3, GameConsts.BoardElementType.Water);
            levels[2] = new Level(3, boardElements);

            if (callback != null)
            {
                callback(levels);
            }
        }
    }
}