﻿namespace Game.GameUtils
{
    public static class GameConsts 
    {
        public const string LEVELS_FILE_NAME = "Levels.json";
        
        public enum BoardElementAnimationTriggers
        {
            Idle,
            Destroy
        }
        
        public enum BoardElementType
        {
            Empty = 0, //empty type added just for more readability json file
            Fire = 1,
            Water = 2
        }
        
        public enum BoardElementState
        {
            Idle = 0,
            Falling = 1,
            Swiping = 2,
            Matched = 3,
            Destroyed = 4,
        }
        
        public enum SwipeDirection
        {
            Up = 0,
            Down = 1,
            Left = 2,
            Right = 3
        }
    }
}